#' @title Mass and Height data
#' @description Mass and Height data from Starwars
#' @format A data frame with 87 rows and 3 variables:
#' \describe{
#'   \item{\code{name}}{character COLUMN_DESCRIPTION}
#'   \item{\code{height}}{double COLUMN_DESCRIPTION}
#'   \item{\code{mass}}{double COLUMN_DESCRIPTION}
#'}
#' @details DETAILS
"donnees_poids"
