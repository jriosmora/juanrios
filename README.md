
<!-- README.md is generated from README.Rmd. Please edit that file -->

# juanrios

<!-- badges: start -->
<!-- badges: end -->

The goal of juanrios is to present 2 pfunctions: imc() and
multi_import_excel()

## Installation

You can install the development version of juanrios like so:

``` r
remotes::install_local(path = 'juanrios_0.xx.0.tar.gz') (exemple : remotes::install_local(path = 'juanrios_0.xx.0.tar.gz_0.1.0.tar.gz'))
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(juanrios)
## basic example code
```
